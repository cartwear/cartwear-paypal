============
Contributing
============

Do this::

    mkvirtualenv cartwear-paypal
    git clone git://github.com/cartwear/cartwear-paypal.git
    cd cartwear-paypal
    make install

then you should be able to run the tests using::

    ./runtests.py

There is also a sandbox site for exploring a sample cartwear site.  Set it up::

    make sandbox

and run it::

    ./manage.py runserver

Use the `Github issue tracker`_ for any problems.

.. _`Github issue tracker`: https://github.com/cartwear/cartwear-paypal/issues
