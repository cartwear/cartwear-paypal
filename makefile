install:
	pip install django-cartwear
	pip install -r requirements.txt
	python setup.py develop

upgrade:
	pip install -U django-cartwear
	pip install -U -r requirements.txt --use-mirrors
	python setup.py develop --upgrade

sandbox: install
	-rm -f sandbox/db.sqlite
	sandbox/manage.py syncdb --noinput
	sandbox/manage.py loaddata sandbox/fixtures/auth.json countries.json
